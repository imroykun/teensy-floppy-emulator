This is an attempt to make a floppy drive emulator using a [Teensy LC](https://www.pjrc.com/store/teensylc.html)
microcontroller board.

It is based on:
 - sundbyk's [Enhanced Gotek](https://easyeda.com/sundbyk/gotek) schematic
 - [this Gotek schematic](https://torlus.com/floppy/forum/download/file.php?id=1071&sid=66f03b2701bb11999d8b357af8d1ef6b) (in negative for some reason)

I chose to use an SD card for storage instead of USB, using the [Teensy SD adaptor circuit](https://www.pjrc.com/teensy/sd_adaptor.html)
as a basis. To make a USB version I would need to use a Teensy 3.6 or maybe 4.0.

The plan is to port [FlashFloppy](https://github.com/keirf/FlashFloppy/) to the Arduino (and/or PlatformIO?) framework in order
to support this and other hardware. Hopefully I/O speed hacks won't be necessary.

This is a work in progress and is completely untested!

[View on CadLab](https://cadlab.io/projects/teensy-floppy-emulator)
